

    <!-- ==========================
        HOME SECTION 
    =========================== -->

    <header id="home" class="home text-center demo-1">
        <div class="over-bg large-header" id="large-header" > 
            <canvas id="demo-canvas"></canvas>
            <!-- //INTRO CONTENT -->
            <div id="intro">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h1 class="text-white"></h1>
                        </div>

                    </div>
                </div>
            </div>
            
            <!-- //MOUSE ICON -->
            <div class="intro-scroll-down">
                <a data-scroll class="scroll-down" href="#service">
                    <span class="mouse">
                        <span class="mouse-dot"></span>
                    </span>
                </a>
            </div>

        
        </div> <!-- //OVER-BG -->
    </header> <!-- //END HEADER -->



