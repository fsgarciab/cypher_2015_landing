    <!-- ==========================
        ABOUT SECTION 
    =========================== -->
    <section id="about" class="about section-padding">
        <div class="container">

            <!--//SECTION INTRO-->
            <div class="col-md-10 col-md-offset-1 text-center">
                <div class="section-intro">

                    <h2 class="section-intro-heading"> {{ trans('messages.landing.about.title') }} </h2>

                    <img src="images/devider-black.png" class="img-responsive center-block devider" alt="devider">

                    <p class="section-intro-description">
                        {{ trans('messages.landing.about.description') }}
                    </p>

                </div>
            </div>
            <!--//END SECTION INTRO-->

            <!--//SECTION CONTENT-->
            <div class="row section-content about-content-container">

                <!-- SINGLE ITEM -->
                <div class="about-item col-md-6 clearfix text-left">
                    <div class="about-icon-wrapper">
                        <i class="fa fa-crop about-icon"></i>
                    </div>
                    <div class="about-text">
                        <h4>{{ trans('messages.landing.about.subtitles.flexibility') }}</h4>
                        <p>
                            {{ trans('messages.landing.about.subtitles_text.flexibility') }}
                        </p>
                    </div>
                </div>
                <!-- //END SINGLE ITEM-->                

                <!-- SINGLE ITEM -->
                <div class="about-item col-md-6 clearfix text-left">
                    <div class="about-icon-wrapper">
                        <i class="fa fa-desktop about-icon"></i>
                    </div>
                    <div class="about-text">
                        <h4>{{ trans('messages.landing.about.subtitles.creativity') }}</h4>
                        <p>
                            {{ trans('messages.landing.about.subtitles_text.creativity') }}
                        </p>
                    </div>
                </div>
                <!-- //END SINGLE ITEM-->                

                <!-- SINGLE ITEM -->
                <div class="about-item col-md-6 clearfix text-left">
                    <div class="about-icon-wrapper">
                        <i class="fa fa-code about-icon"></i>
                    </div>
                    <div class="about-text">
                        <h4>{{ trans('messages.landing.about.subtitles.quality') }}</h4>
                        <p>
                            {{ trans('messages.landing.about.subtitles_text.quality') }}
                        </p>
                    </div>
                </div>
                <!-- //END SINGLE ITEM-->                

                <!-- SINGLE ITEM -->
                <div class="about-item col-md-6 clearfix text-left">
                    <div class="about-icon-wrapper">
                        <i class="fa fa-compass about-icon"></i>
                    </div>
                    <div class="about-text">
                        <h4>{{ trans('messages.landing.about.subtitles.motivation') }}</h4>
                        <p>
                            {{ trans('messages.landing.about.subtitles_text.motivation') }}
                        </p>
                    </div>
                </div>
                <!-- //END SINGLE ITEM-->                

            </div>
            <!-- //END SECTION CONTENT -->

        </div>
        <!-- //END CONTAINER -->
    </section>
    <!-- //END ABOUT SECTION -->