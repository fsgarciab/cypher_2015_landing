



    <!-- ==========================
        SERVICE SECTION 
    =========================== -->
    <section id="service" class="service section-padding">
        <div class="container">

            <div class="row">

                <!--//SECTION INTRO-->
                <div class="section-intro">
                    <div class="col-md-8 col-md-offset-2 text-center">

                        <h2 class="section-intro-heading"> {{ trans('messages.landing.service.title') }} </h2>

                        <img src="images/devider-black.png" class="img-responsive center-block devider" alt="devider">

                        <p class="section-intro-description">
                            {{ trans('messages.landing.service.description') }}
                        </p>

                    </div>
                </div>
                <!--//END SECTION INTRO-->

            </div> 
            <!-- //END ROW -->


            <!--//SECTION CONTENT-->
            <div class="section-content service-content-container">

                <div class="row">
                    
                    <!-- //SINGLE SERVICE -->
                    <div class="single-service col-md-4 text-left">

                        <div class="col-md-2 single-service-icon">
                            <i class="fa fa-eye"></i>
                        </div>                        

                        <div class="col-md-10 single-service-content">
                            <h4>{{ trans('messages.landing.service.subtitles.design') }}</h4>
                            <p>
                                {{ trans('messages.landing.service.subtitles_text.design') }}
                            </p>
                        </div>

                    </div>
                    <!-- //END SINGLE SERVICE -->  
                    
                    <!-- //SINGLE SERVICE -->
                    <div class="single-service col-md-4 text-left">

                        <div class="col-md-2 single-service-icon">
                            <i class="fa fa-pencil-square-o"></i>
                        </div>                        

                        <div class="col-md-10 single-service-content">
                            <h4>{{ trans('messages.landing.service.subtitles.optimization') }}</h4>
                            <p>
                                {{ trans('messages.landing.service.subtitles_text.optimization') }}
                            </p>
                        </div>

                    </div>
                    <!-- //END SINGLE SERVICE -->  
                    
                    <!-- //SINGLE SERVICE -->
                    <div class="single-service col-md-4 text-left">

                        <div class="col-md-2 single-service-icon">
                            <i class="fa fa-dashcube"></i>
                        </div>                        

                        <div class="col-md-10 single-service-content">
                            <h4>{{ trans('messages.landing.service.subtitles.development') }}</h4>
                            <p>
                                {{ trans('messages.landing.service.subtitles_text.development') }}
                            </p>
                        </div>

                    </div>
                    <!-- //END SINGLE SERVICE -->  

                </div>
                <!-- //END ROW -->
                
                <div class="row">

                    <!-- //SINGLE SERVICE -->
                    <div class="single-service col-md-4 text-left">

                        <div class="col-md-2 single-service-icon">
                            <i class="fa fa-comments-o"></i>
                        </div>                        

                        <div class="col-md-10 single-service-content">
                            <h4>{{ trans('messages.landing.service.subtitles.advice') }}</h4>
                            <p>
                                {{ trans('messages.landing.service.subtitles_text.advice') }}
                            </p>
                        </div>

                    </div>
                    <!-- //END SINGLE SERVICE -->
                    
                    <!-- //SINGLE SERVICE -->
                    <div class="single-service col-md-4 text-left">

                        <div class="col-md-2 single-service-icon">
                            <i class="fa fa-bitbucket"></i>
                        </div>                        

                        <div class="col-md-10 single-service-content">
                            <h4>{{ trans('messages.landing.service.subtitles.analysis') }}</h4>
                            <p>
                                {{ trans('messages.landing.service.subtitles_text.analysis') }}
                            </p>
                        </div>

                    </div>
                    <!-- //END SINGLE SERVICE -->                    
                    
                    <!-- //SINGLE SERVICE -->
                    <div class="single-service col-md-4 text-left">

                        <div class="col-md-2 single-service-icon">
                            <i class="fa fa-check-square-o"></i>
                        </div>                        

                        <div class="col-md-10 single-service-content">
                            <h4>{{ trans('messages.landing.service.subtitles.shopping') }}</h4>
                            <p>
                                {{ trans('messages.landing.service.subtitles_text.shopping') }}
                            </p>
                        </div>

                    </div>
                    <!-- //END SINGLE SERVICE -->


                </div>
                <!-- //END ROW -->


            </div>
            <!-- //END SECTION CONTENT -->

        </div>
        <!-- //END CONTAINER -->
    </section>
    <!-- //END SERVICE SECTION -->


