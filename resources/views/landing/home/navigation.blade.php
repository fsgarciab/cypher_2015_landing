
    <!-- ==========================
        NAVIGATION 
    =========================== -->    

    <!-- NAVIGATION START -->
    <div class="navigation-header">
        
        <!-- STICKY NAVIGATION -->
        <div class="navbar navbar-default navbar-fixed-top" role="navigation" data-spy="affix" data-offset-top="50">
            <div class="container">
                <div class="navbar-header">
                    
                    <!-- LOGO ON STICKY NAV BAR -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#GraphX-navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="images/logo-cypher-white.png" alt=""></a>
                    
                </div>
                
                <!-- NAVIGATION LINKS -->
                <div class="navbar-collapse collapse" id="GraphX-navigation">
                    <ul class="nav navbar-nav navbar-right main-navigation">
                        <li><a data-scroll href="#home">{{ trans('messages.landing.navigation.home') }}</a></li>
                        <li><a data-scroll href="#service">{{ trans('messages.landing.navigation.services') }}</a></li>
                        <li><a data-scroll href="#subcriptions">{{ trans('messages.landing.navigation.subscription') }}</a></li>
                        <li><a data-scroll href="#about">{{ trans('messages.landing.navigation.about') }}</a></li>
                        <li><a data-scroll href="#contact-area">{{ trans('messages.landing.navigation.contact') }}</a></li>
                    </ul>
                </div>
                
            </div>
            <!-- //END CONTAINER -->
            
        </div>
        <!-- //END STICKY NAVIGATION -->
        
        
    </div>
    <!-- //END NAVIGATION -->

