



    <!-- ==========================
        FOOTER SECTION 
    =========================== -->
    <footer class="footer section-padding"> 
        <div class="container">

            <div class="row">

                <!--//SECTION INTRO-->
                <div class="section-intro">
                    <div class="col-md-8 col-md-offset-2 text-center">

                        <h2 class="section-intro-heading"> {{ trans('messages.landing.footer.title') }} </h2>

                        <p class="section-intro-description">
                            {{ trans('messages.landing.footer.description') }}
                        </p>

                    </div>
                </div>
                <!--//END SECTION INTRO-->

            </div> 
            <!--//END ROW --> 




            <div class="row text-center">
                
                           
                
                <!--//SINGLE ITEM-->
                <div class="col-md-6 social-footer"> 
                    <i class="fa fa-comments-o"></i> <br>
                    <span>Contacto</span> <br>
                    <h4><a href="mailto: contacto@agenciacypher.com"> contacto@agenciacypher.com</a></h4> 
                </div>
                <!--//END SINGLE ITEM-->                
                
                <!--//SINGLE ITEM-->
<!--                <div class="col-md-2 social-footer"> 
                    <i class="fa fa-twitter"></i> <br>
                    <span>Twitter</span> <br>
                    <h4><a href="#">@Twitter</a></h4>
                </div>-->
                <!--//END SINGLE ITEM-->                
                
                <!--//SINGLE ITEM-->
<!--                <div class="col-md-2 social-footer"> 
                    <i class="fa fa-facebook"></i> <br>
                    <span>facebook</span> <br>
                    <h4><a href="#">@facebook</a></h4>
                </div>-->
                <!--//END SINGLE ITEM-->
                
                <!--//SINGLE ITEM-->
<!--                <div class="col-md-2 social-footer"> 
                    <i class="fa fa-codepen"></i> <br>
                    <span>codepen</span> <br>
                    <h4><a href="#">@codepen</a></h4>
                </div>-->
                <!--//END SINGLE ITEM--> 
                
                <!--//SINGLE ITEM-->
                <div class="col-md-6 social-footer"> 
                    <i class="fa fa-whatsapp"></i> <br>
                    <span>Whatsapp</span> <br>
                    <h4><a>0987915611</a></h4>
                    <h4><a>0984443800</a></h4>
                </div>
                <!--//END SINGLE ITEM-->  

            </div>

            <hr class="footer-line">

            <div class="copyright text-center"> {{ trans('messages.landing.footer.copy') }}</div>

        </div>
    </footer>
    <!--//END FOOTER SECTION-->


