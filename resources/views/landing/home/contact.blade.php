

    <!-- ==========================
        CONTACT SECTION 
    =========================== -->
    <section id="contact-area" class="contact section-padding">
        <div class="container">

            <div class="row">

                <!--//SECTION INTRO-->
                <div class="section-intro">
                    <div class="col-md-8 col-md-offset-2 text-center">

                        <h2 class="section-intro-heading"> {{ trans('messages.landing.contact.title') }}  </h2>

                        <img src="images/devider-black.png" class="img-responsive center-block devider" alt="devider">

                        <p class="section-intro-description">
                            {{ trans('messages.landing.contact.description') }}
                        </p>

                    </div>
                </div>
                <!--//END SECTION INTRO-->

            </div> 
            <!-- //END ROW -->            



            <!--//CONTACT INTRO-->
            <div class="contact-info row">
                <div class="col-md-10 col-md-offset-1 text-center">
                    <div class="col-md-6 single-contact-info">
                        <i class="fa fa-envelope-o"></i>
                        <h5> <a href="mailto: contacto@agenciacypher.com">contacto@agenciacypher.com</a> </h5>
                    </div>                        

<!--                    <div class="col-md-4 single-contact-info">
                        <i class="fa fa-map-marker"></i>
                        <h5>{{ trans('messages.landing.contact.address') }}</h5>
                    </div>                        -->

                    <div class="col-md-6 single-contact-info">
                        <i class="fa fa-whatsapp"></i>
                        <h5>0987915611</h5>
                        <h5>0984443800</h5>
                    </div>
                </div>
            </div>
            <!--//END CONTACT INTRO-->





            <!--//SECTION CONTENT CONTAINER-->
            <div class="section-content-container">
                <div class="container">

                    <div class="col-md-10 col-md-offset-1 text-center">

                        <form id="contact" role="form" >

                            <!-- IF MAIL SENT SUCCESSFULLY -->
                            <h6 class="success"><i class="fa fa-check"></i> {{ trans('messages.landing.contact.form.success') }} </h6>
                            
                            <!-- IF MAIL SENDING UNSUCCESSFULL -->
                            <h6 class="error"><i class="fa fa-times"></i> {{ trans('messages.landing.contact.form.error') }} </h6>

                            <div class="row">

                                <div class="col-lg-6">
                                    
                                    <!-- NAME -->
                                    <div class="form-group">
                                        <label for="cf-name" class="control-label hide">Name</label>
                                        <input class="form-control input-box" id="cf-name" type="text" name="cf-name" placeholder="{{ trans('messages.landing.contact.form.name') }}" />
                                    </div>
                                    
                                    <!-- EMAIL -->
                                    <div class="form-group">
                                        <label for="cf-email" class="control-label hide">Email</label>
                                        <input class="form-control input-box" id="cf-email" type="email" name="cf-email" placeholder="Email" />
                                    </div>

                                    <!-- SUBJECT -->
                                    <div class="form-group">
                                        <label for="cf-subject" class="control-label hide">Subject</label>
                                        <input class="form-control input-box" id="cf-subject" type="text" name="cf-subject" placeholder="{{ trans('messages.landing.contact.form.subject') }}" />
                                    </div>
                                    
                                </div>

                                <div class="col-lg-6">

                                    <!-- MASSAGE -->
                                    <div class="form-group">
                                        <label for="cf-message" class="control-label hide">Message</label>
                                        <textarea class="form-control textarea-box" id="cf-message" rows="8" name="cf-message" placeholder="{{ trans('messages.landing.contact.form.message') }}"></textarea>
                                    </div>
                                    
                                    <!-- BUTTON -->
                                    <div class="form-group text-right">
                                        <button type="submit" id="cf-submit" name="submit" class="btn primary-button default-button">{{ trans('messages.landing.contact.form.button') }}</button>
                                    </div>

                                </div>

                            </div>

                        </form>

                    </div>
                    
                </div>
                <!--//END CONTAINER-->            
            </div>
            <!--//END SECTION CONTENT CONTAINER-->


        </div>
        <!--//END CONTAINER-->
    </section>
    <!--//END CONTACT SECTION-->