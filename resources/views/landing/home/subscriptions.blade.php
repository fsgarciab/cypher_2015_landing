


    <!-- ==========================
        SUBCRIPTIONS SECTION 
    =========================== -->
    <section id="subcriptions" class="subcriptions">
        <div class="over-bg section-padding">
            <div class="container">

                <div class="row">

                    <!--//SECTION INTRO-->
                    <div class="section-intro">
                        <div class="col-md-8 col-md-offset-2 text-center">

                            <h2 class="section-intro-heading text-white"> {{ trans('messages.landing.subscriptions.title') }} </h2>

                            <img src="images/devider-white.png" class="img-responsive center-block devider" alt="devider">

                            <p class="section-intro-description text-white">
                                {{ trans('messages.landing.subscriptions.description') }}
                            </p>

                        </div>
                    </div>
                    <!--//END SECTION INTRO-->
                </div> 
                <!-- //END ROW -->


                <div class="row">

                    <!--//SECTION CONTENT-->
                    <div class="col-md-8 col-md-offset-2 text-center section-content subcriptions-content-container">
                       
                        <!-- SUBSCRIPTION SUCCESS MESSAGES -->
                        <h5 class="text-white subscription-success"></h5>
                        <h5 class="text-white subscription-error"></h5>
                        <br>
                        <!-- SUBCRIPTION CONTENT -->
                        <div class="col-sm-10 col-sm-offset-1">
                            <div class="horizontal-subscribe-form">

                                <form class="form-inline mailchimp">

                                    <input type="email" id="subscriber-email" name="email" class="form-control input-box" placeholder="{{ trans('messages.landing.subscriptions.labels.placeholder') }}">

                                    <button type="submit" class="btn btn-primary default-button">{{ trans('messages.landing.subscriptions.labels.button_label') }}</button>
                                    
                                </form>
                            </div>
                        </div>


                    </div>
                    <!-- //END SECTION CONTENT -->

                </div> 
                <!-- //END ROW -->


            </div>
            <!-- //END CONTAINER -->
        </div>
        <!-- //END OVER BG -->
    </section>
    <!-- //END SUBCRIPTIONS SECTION -->


