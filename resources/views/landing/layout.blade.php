<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="UTF-8">
    <meta name="description" content="{{ trans('messages.landing.description') }}">
    <meta name="keywords" content="{{ trans('messages.landing.keywords') }}">
    <meta name="author" content="CYPHER">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- SITE TITLE -->
    <title> {{ trans('messages.landing.title') }}</title>

    <!-- =========================
          FAV AND TOUCH ICONS  
    ============================== -->
    <link rel="icon" href="{{ asset('/images/favicon.ico') }}">
    <link rel="apple-touch-icon" href="{{ asset('/images/apple-touch-icon.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/images/apple-touch-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('/images/apple-touch-icon-114x114.png')}}">

    <!-- =========================
         GOOGLE FONTS   
    ============================== -->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic' rel='stylesheet'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,400italic,700' rel='stylesheet'>

    <!-- =========================
        STYLESHEETS   
    ============================== -->
    
    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/responsive.css') }}" rel="stylesheet">


    <!-- COLORS -->
    <link rel="stylesheet" href="{{ asset('/css/colors/blue.css') }}"> <!-- DEFAULT COLOR/ CURRENTLY USING -->
<!--     <link rel="stylesheet" href="css/colors/red.css"> -->
    <!-- <link rel="stylesheet" href="css/colors/green.css"> -->
    <!-- <link rel="stylesheet" href="css/colors/purple.css"> -->
    <!-- <link rel="stylesheet" href="css/colors/orange.css"> -->
    <!-- <link rel="stylesheet" href="css/colors/blue-munsell.css"> -->
    <!-- <link rel="stylesheet" href="css/colors/slate.css"> -->
    <!-- <link rel="stylesheet" href="css/colors/yellow.css"> -->

    <!-- FOOTER COLOR -->
    <!-- <link href="css/colors/footer-color.css" rel="stylesheet"> -->


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>

  <body data-spy="scroll" data-target=".navbar-default" data-offset="100">
    
    <!-- =========================
         PRE LOADER       
    ============================== -->
    <div class="preloader">
      <div class="status">&nbsp;</div>
    </div>

    <!-- ==========================
        NAVIGATION 
    =========================== -->    

    <!-- NAVIGATION START -->
    @include('landing.home.navigation')
    <!-- //END NAVIGATION -->



    <!-- ==========================
        HOME SECTION 
    =========================== -->

    @include('landing.home.banner')



    <!-- ==========================
        SERVICE SECTION 
    =========================== -->
   @include('landing.home.service')
    <!-- //END SERVICE SECTION -->

    



    <!-- ==========================
        SUBCRIPTIONS SECTION 
    =========================== -->
    @include('landing.home.subscriptions')
    <!-- //END SUBCRIPTIONS SECTION -->



    <!-- ==========================
        ABOUT SECTION 
    =========================== -->
   @include('landing.home.about')
    <!-- //END ABOUT SECTION -->

    <!-- ==========================
        CONTACT SECTION 
    =========================== -->
   @include('landing.home.contact')
    <!--//END CONTACT SECTION-->


    <!-- ==========================
        FOOTER SECTION 
    =========================== -->
   @include('landing.home.footer')
    <!--//END FOOTER SECTION-->



    <!-- ==========================
        JAVASCRIPT 
    =========================== -->

   
 <script src="{{ asset('/js/jquery.min.js') }}"></script>
    <script>
        
         var CYPHER = CYPHER || {};                    
            CYPHER.url = '{{ url("/") }}';
            
            CYPHER.csrf_token='{{ csrf_token() }}';
       
    </script>

    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/js/smooth-scroll.js') }}"></script>
    <script src="{{ asset('/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('/js/animatedbg.js') }}"></script>
    <script src="{{ asset('/js/script.js') }}"></script>
    <script src="{{ asset('/js/jquery.plugin.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.countdown.min.js') }}"></script>


  </body>
</html>