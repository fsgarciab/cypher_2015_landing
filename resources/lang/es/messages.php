<?php

return [

    'landing' =>
    [
        'title' => 'CYPHER',
        'description' => 'Empresa Desarrollo y Servicios Tecnológicos',
        'keywords' => 'software,redes sociales,soluciones,paginas web',
        'navigation' =>
        [
            'home' => 'Inicio',
            'about' => 'Nosotros',
            'subscription' => 'Suscribase',
            'services' => 'Servicios',
            'contact' => 'Contáctanos',
        ],
        'about' =>
        [
            'title' => 'SOBRE NOSOTROS',
            'description' =>'Somos 2 emprendedores Ecuatorianos geeks de la tecnología con el sueño de influenciar positivamente el mercado con grandes ideas plasmadas en proyectos. Creemos en la positividad y trabajo duro de los jovenes emprendedores, por lo que trabajamos en conjunto con otras empresas emprendedoras del mercado para entregar los mejores y más creativos productos a todos nuestros clientes.',
            'subtitles' =>
            [
                'flexibility' => 'Flexibilidad',
                'creativity' => 'Creatividad',
                'quality' => 'Calidad',
                'motivation' => 'Motivación',
            ],
            'subtitles_text' =>
            [
                'flexibility' => 'Somos flexibles con los proyectos que desarrollamos. Nos guíamos con metodologías para afrontar los cambios de la mejor manera posible y de esta manera superar las expectativas de los clientes',
                'creativity' => 'Formamos parte de un grupo de emprendedores con grandes cualidades creativas que hacen de nosotros un gran ejemplo de calidad visual en cada uno de los proyectos que desarrollamos.',
                'quality' => 'Ponemos en práctica las mejores metodologías de gestión de la calidad en el desarrollo de sistemas y aplicaciones ya que esto demuestra nuestro compromiso con nuestro trabajo y con nuestros clientes. ',
                'motivation' => 'Nuestra principal motivación es plasmar nuestras ideas y que sean de ayuda para la sociedad ya sea en la mejora de los procesos de una empresa como en ayudar a mejorar nuestro diario vivir. ',
            ]
        ],
        'subscriptions' =>
        [
            'title' => 'Suscribase a nuestros boletines',
            'description' => 'Queremos compartir nuestro conocimiento con cualquier persona interesada en la tecnología: Noticias de innovación, herramientas y tips tecnológicos, etc. ',
            'labels' =>
            [
                'placeholder' => 'Ingrese su Email',
                'button_label' => 'ENVIAR'
            ]
        ],
        'service' =>
        [
            'title' => 'NUESTROS SERVICIOS',
            'description' => 'Ofrecemos varios servicios tanto tradicionales como innovadores. Además contamos con un grupo de emprendedores aliados que nos permiten entregar soluciones de calidad a nuestros clientes.',
            'subtitles' =>
            [
                'design' => 'DISEÑO CREATIVO',
                'optimization' => 'OPTIMIZACIÓN DE APLICACIONES',
                'development' => 'DESARROLLO DE APLICACIONES',
                'advice' => 'ASESORÍA TÉCNICA',
                'analysis' => 'ANALISIS DE DATOS',
                'shopping' => 'VENTA EN LÍNEA',
            ],
            'subtitles_text' =>
            [
                'design' => 'El diseño es la mejor manera de llegar a los clientes, por eso nuestro grupo de aliados harán de tu imagen empresarial algo innovador y llamativo.',
                'optimization' => 'Para todos esos sistemas y aplicaciones que necesitan optimización una reingeniería. Analizamos tus procesos para mejorarlos.',
                'development' => 'Nos encanta trabajar en todo tipo de desarrollo, ya sean tus proyectos para la web o para un dispositivo móvil.',
                'advice' => 'Si necesitas solamente una guía tecnológica estamos para ayudarte. Suscribete o contactanos estamos felices de ayudarte con esas preguntas técnicas.',
                'analysis' => 'No entregamos informes planos y aburridos, sino que vas a ver las estadísticas de segmentación y posibles soluciones de una forma gráfica e innovadora.',
                'shopping' => 'Si lo que buscas es una aplicación o una solución de venta en línea, contactanos. Tenemos varias soluciones que te podrían interesar.',
            ],
        ],
        'contact' =>
        [
            'title' => 'CONTÁCTANOS',
            'description' => 'Si tienes cualquier duda o necesitas una cotización, ponte en contacto con nosotros.',
            'address' => 'Glen Road, E13 8 London, UK',
            'form' =>
            [
                'name' => 'Nombres',
                'subject' => 'Asunto',
                'message' => 'Mensaje...',
                'button' => 'ENVIAR MENSAJE',
                'success' => 'Tu mensaje fue enviado correctamente.',
                'error' => 'La dirección de correo debe ser válida y el mensaje debe contener al menos 1 caracter.'
            ]
        ],
        'footer' =>
        [
            'title' => 'MANTENTE EN CONTACTO',
            'description' => 'Siguenos en cualquiera de nuestras redes sociales y entérate de nuestros productos y servicios.',
            'copy' => '&copy; 2015 CYPHER. Todos los Derechos Reservados'
        ]
    ],
 

];
