<?php

namespace App\Http\Controllers\Landing;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use App\Models\Landing\ContactForm;
use Illuminate\Support\Facades\Response;


class HomeController extends BaseController
{
    public function getHome(){
        return view('landing.layout');
    }
    
    public function getChangeLanguage($locale){
        App::setLocale($locale);
    }
    
    public function saveContactForm(){
        $name=Input::get('name');
        $subject=Input::get('subject');
        $email=Input::get('email');
        $message=Input::get('message');
        
        $formulario=new ContactForm();
        $formulario->name=$name;
        $formulario->subject=$subject;
        $formulario->email=$email;
        $formulario->message=$message;
        $formulario->save();
        return Response::json(['ok']);
    }
}
