<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

 
Route::group(['namespace' => 'Landing','middleware' => ['web']], function()
  { 
          Route::get('/', ['uses' => 'HomeController@getHome']); 
          Route::post('guardarDatosContacto', ['uses' => 'HomeController@saveContactForm']); 
  });


