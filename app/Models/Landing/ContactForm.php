<?php

namespace App\Models\Landing;
use Illuminate\Database\Eloquent\Model;
use Validator;

class ContactForm extends Model{

    protected $table = 'contact_form';

     
    public static function validate($post)
    {
        
            $rules = array(
            'name' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'message' => 'required',
                
            ); 

        
        

        $validator = Validator::make($post, $rules);

        return $validator;
    }

}